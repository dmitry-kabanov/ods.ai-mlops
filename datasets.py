import tensorflow as tf

from numpy import ndarray
from typing import Tuple


def load_mnist() -> Tuple[Tuple[ndarray, ndarray], Tuple[ndarray, ndarray]]:
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
    assert x_train.shape == (60000, 28, 28)
    assert x_test.shape == (10000, 28, 28)

    return (x_train, y_train), (x_test, y_test)


def preprocess_features(x_train: ndarray, x_test: ndarray) -> Tuple[ndarray, ndarray]:
    return x_train / 255.0, x_test / 255.0

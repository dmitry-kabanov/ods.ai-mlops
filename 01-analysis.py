# %% [markdown]
# # Analysis of the dataset
# ## Imports

# %%
import matplotlib.pyplot as plt
import tensorflow as tf

# %% [markdown]
# ## Load data

# %%
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
assert x_train.shape == (60000, 28, 28)
assert x_test.shape == (10000, 28, 28)

# %% [markdown]
# ## Plot inputs

# %%
idx_list = [0, 20000, 24585, 30000, 58000, 58500]

fig, axes = plt.subplots(2, 3)
for i in range(2):
    for j in range(3):
        k = i * 3 + j
        axes[i, j].imshow(x_train[k], cmap="viridis")
        axes[i, j].set_title(y_train[k])

fig.tight_layout()

# %% [markdown]
# ## Data preprocessing

# %%
x_train = x_train / 255.0
x_test = x_test / 255.0
